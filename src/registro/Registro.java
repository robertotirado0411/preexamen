/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registro;
import java.util.Scanner;

/**
 *
 * @author Edgar Guerrero
 */
public class Registro {
    private int codigoVenta;
    private int cantidad;
    private int tipo;
    private float precio; 

    public Registro(int codigoVenta, int cantidad, int tipo, float precio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    
    
    
    public Registro(Registro otro){
        this.cantidad=otro.cantidad;
        this.codigoVenta=otro.codigoVenta;
        this.precio=otro.precio;
        this.tipo=otro.tipo;
        
    }

    public Registro() {
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }


    public float calcularCostoVenta(){
        float costoVenta = 0.0f;
        costoVenta = this.cantidad * this.precio;
        return costoVenta;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularCostoVenta()*.16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float totalPagar = 0.0f;
        totalPagar = this.calcularCostoVenta()+ this.calcularImpuesto();
        return totalPagar;
    }    
    

        
    public void imprimirRegistro(){
        System.out.println("Codigo de venta: " + this.codigoVenta);
        System.out.println("Cantidad: " + this.cantidad);
        System.out.println("Tipo : " + this.tipo    );
        System.out.println("Precio : " + this.precio);
        System.out.println("Costo de venta : " + this.calcularCostoVenta());
        System.out.println("Impuesto : " + this.calcularImpuesto());
        System.out.println("Total pagar : " + this.calcularTotalPagar());
        
    }
    
    
    
}

